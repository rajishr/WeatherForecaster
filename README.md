# Weatherforecast

This module is a model mimicing the functioning of a weather forecasting engine. The forecasting is based on an assumption that the weather trends repeat itself if not exactly but to a great similarity.  It is a mathematical model inspired by the basic concepts of mean, variance and standard deviation. 

Since there are no sensor data available for this module to provide real-time or futuristic weather factors we rely upon historical weather data to set the model running. 



## Pre-Requisites
#### Install Java
To run the application java 1.7 or version above needs to be installed in the machine.
#### Install Maven
##### Following is the installation process:
 * Download Apache Maven and install it.
 * Unzip apache-maven-3.3.9-bin.zip
 * Add the bin directory of the created directory apache-maven-3.3.9 to the PATH environment variable.

#### Project setup
 * Ensure the history data is downloaded to provide input ([download here](https://www.wunderground.com)) or you can find the same in the following path within the project "weatherforecast/src/main/resources/input".
 * Ensure the configuration and properties files are updated. Details in the configuration section of Approach note below.
 * Ensure that the following jar is built and used for execution:

```
weatherforecast.jar
```
A versioned jar with a similar name would be found in the target folder which does not have the Main Class preset.

## Execution Procedure
#### Modes

This module works in 3 modes:

 * -a => Forecasting for all future dates including today. 
 * -d => Forecasting for a specific date queried for. 
 * -r => Forecasting for a range of dates between queried start and end dates.

#### Command to Run
```
java -jar weatherforecast.jar <mode> <properties_file_path> [<start_date>] [<end_date>]
```
Eg. below:
```
java -jar weatherforecast.jar -a /home/860735/Desktop/test/config/weatherinfo.properties
```
1. mode - Mode parameter
2. properties_file_path- absolute path of properties file
3. start_date- 
 	* for mode -d => specific date to be forecasted 
 	* for mode -r => starting date of date range
4. end_date- 
 	* for mode -r => end date of forecast

Note: The date format is yyyy-M-d, for eg. 2016-11-5


## Dataset Considered
Historical weather data between 2005 and 2015 (both years inclusive) for 4 locations listed below:

 * Sydney
 * Melbourne
 * Adelaide
 * Brisbane

source: [Weather Underground](https://www.wunderground.com)

## Approach Note

#### Assumptions:
 1. The weather on a day of the year bears resemblance to the trend of change in weather of the same day in earlier years.
 2. Prediction over predicted data might arguably reduce the accuracy of prediction. Also since we are using the system memory (heap) instead of a database to hold the historical data, the model might fail at a specific time when the data generation crosses the system limits.
 
#### Configuration:

The configuration file "locationinformation.xml" holds the static information regarding various geographic locations (weather stations) which are subject to weather forecasting. The configuration file reads as below:

PATH: weatherforecast/src/main/resources/config/locationinformation.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<locations>
	<location city="SYDNEY" iatacode="SYD" countrycode="AU" cordinates="-33.86,151.21" altitude="39">
		<historydata dir="/home/860735/Desktop/Projects/CBACaseStudy/Data/Sydney" />
	</location>
	<location city="MELBOURNE" iatacode="MEL" countrycode="AU" cordinates="-37.83,144.98" altitude="7">
		<historydata dir="/home/860735/Desktop/Projects/CBACaseStudy/Data/Melbourne" />
	</location>
	<location city="ADELAIDE" iatacode="ADL" countrycode="AU" cordinates="-34.92,138.62" altitude="48">
		<historydata dir="/home/860735/Desktop/Projects/CBACaseStudy/Data/Adelaide" />
	</location>
	<location city="BRISBANE" iatacode="BRS" countrycode="AU" cordinates="-27.47,153.02" altitude="27">
		<historydata dir="/home/860735/Desktop/Projects/CBACaseStudy/Data/Brisbane" />
	</location>
</locations>
```
The above xml adheres to a schema document "locationinformation.xsd" as follows:

PATH: weatherforecast/src/main/resources/config/locationinformation.xsd
```xml
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
  <xs:element name="locations">
    <xs:complexType>
      <xs:sequence>
        <xs:element maxOccurs="unbounded" ref="location"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  <xs:element name="location">
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="historydata"/>
      </xs:sequence>
      <xs:attribute name="altitude" use="required" type="xs:integer"/>
      <xs:attribute name="city" use="required" type="xs:NCName"/>
      <xs:attribute name="cordinates" use="required"/>
      <xs:attribute name="countrycode" use="required" type="xs:NCName"/>
      <xs:attribute name="iatacode" use="required" type="xs:NCName"/>
    </xs:complexType>
  </xs:element>
  <xs:element name="historydata">
    <xs:complexType>
      <xs:attribute name="dir" use="required"/>
    </xs:complexType>
  </xs:element>
</xs:schema>

```
#### Properties:
The following properties help execute the module:

PATH: weatherforecast/src/main/resources/config/weatherinfo.properties
```
CONFIG_LOCATION=/home/860735/cbaworkspace/weatherforecast/src/main/resources/config/locationinformation.xml
GLOBAL_WARMING_COEFF=6
```
The CONFIG_LOCATION is the location of "locationinformation.xml" which holds the static details of the weather stations.

The GLOBAL_WARMING_COEFF is introduced to make the model more realistic as the weather varies gradually year after year due to Global warming which can be plotted on a logarithmic scale. Increasing this coefficient slows down the pace of temperature rise while decreasing the same would shoot the temperature rise. The current value '6' is an optimal value based upon theoritical study which states that the temperature rise in the next 40 years would be between 0.4 degree to 2.0 degree on the celcius scale.


#### Functional Description:
The historical data is loaded into memory in a manner such that whenever a query for forecasting weather for any day in the year, is made, it fetches the weather information from the previous years for the same day as reference. This data is shuffled and a window slice of 3 historical weather information is selected to process the variance-deviation model.

[Note: For the leap years alone, we consider the 366th and 365th day for analysis. This is because we only have 2 historical data on 366th day]

The model finds the average or mean value for each weather factors (temperature, pressure and humidity) from the above filtered data. 
```
Mean(μ) = ∑(Xij)/j => (X(i)+X(i+1)....X(j))/j 
```
 ; where j is the total number of samples
 
It then fetches the area of distribution of each weather factor (which we call the variance).  

```
Variance(σ^2) = ∑ij[f(X(ij),μ)]/j 
```
Here,
```
f(X(ij),μ) = (Xi-μ)^2
```

For each of the selected weather information items, we find the variance polarity, i.e the direction of trend which comes as positive,negative or neutral, as it deviates from the mean. Finally, the standard deviation is found, which ideally provides the optimal range within which the weather factor has been distributed. 
```
(σ)=SQRT(Variance)
```
; where SQRT is the square root function.

The above deviation, again, may be in a any polarity, hence indicating the significance of variance polarity.

The deviation from the mean (μ), so found, is calculated considering the polarity of variance. This decides the final value of the weather factor.

To make the model more realistic as the weather varies gradually year after year due to Global warming which can be plotted on a logarithmic scale. The following formula was derived based on various assumptions related to Global warming and its effect on weather factors.

```
Δt = (δ/<GLOBAL_WARMING_COEFF>)*Math.log10(2)
```
Here, 
 * Δt is the factorial increase in temperature. 
 * δ is the difference in years from the last year in historical data.
 * GLOBAL_WARMING_COEFF is the factor governing the pace of global warming.

This factor gets added to the temperature.

Finally, based on the values forecasted for the weather factors, the weather type is categorized as Cloudy, Rainy, Cool, Cold, Warm and Hot.

#### Technical Description:
##### Classes of Significance
 1. WeatherForecaster.java - This class acts as the starting point of the application. it loads the configuration file using JAXB and spawns a thread for each work station.
 2. ForecastThread.java - This is a thread class that runs for each workstation configured in the configuration file.
 3. VarianceModelImpl.java - This class does the core functional implementation and has the process to fetch the weather factors for any date.
 4. AlgorithmsUtil.java - The mathematical logic is embedded in this class.
 5. WeatherDetailsBean.java - This is the model (DTO) class carrying the weather information for any day of any year.

##### Process Flow
Having ensured that the directory locations mentioned in the "dir" attribute of the "historydata" tag in xml has the right set of history data in files having per year data, the process is triggered by the WeatherForecaster.java.  The configuration file is loaded into the JAXB class instances. The rest of the process works in a multi-threaded system where a single thread encompasses all the data loading, processing and prediction for a single location.

The ForecasThread.java class is the code bearer for the threads implementing the Runnable interface. The multithreading is implemented using ExecutorService, as ExecutorService abstracts away many of the complexities associated with the lower-level abstractions like raw Thread. It provides mechanisms for safely starting, closing down, submitting, executing, and blocking on the successful or abrupt termination of tasks (expressed as Runnable or Callable). It also gives a flexible thread pooling.

The core logic is designed to calculate the weather factor values for a single day only and this lies in the VarianceModelImpl.java. The thread appropriately calls the same method in a way to run in 3 modes mentioned earlier in the "Execution Procedure" Section. The mathematical calculations behind the model are contained in the AlgorithmsUtil.java which is called in by the former.

The final weather factors forecasted is collected and based on predefined conditions classified into 6 weather types mentioned earlier. Finally the thread emits the result in string format to the standard output media (console by default) in the following format:

```
ADL|-34.92,138.62,48|2020-12-17T12:00:00Z|HOT|+28.4|1014.5|53
```
Here, the output can be read in the following format:

```
<IATA_CODE>|latitude,longitude,altitude|date_time|Weather_Type|temperature|pressure|relative_humidity
```

## Challenges
1. The accuracy of the model is restricted to the minimal historical data.
2. Logically, the recent weather factors affect the future weather factors as compared to the very early years. This is not currently handled in the model. 

Note: The second challenge might be resolved by providing weightage to recent weather factors but this might bias the system to have close resemblance to the recent data.

## References
#### TestCases:
JUnit test cases are provided in the path 
```
"weatherforecast/src/test/java"
```
which would run on build using Maven too.
#### Documentation:
Please find the documentation in the following paths:
```
"weatherforecast/documents/javadoc" => Auto-generated javadoc
```
Also,
```
"weatherforecast/documents/weatherforecast.html" => Detailed documentation
```
#### Samples:
The sample input and output could be located  in the following paths:
```
weatherforecast/src/test/resources/sampleinput
```
```
weatherforecast/src/test/resources/sampleoutput
```
