/**
 * Copyright (c) 2016, Rajish R. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package com.tcs.cba.weatherforecast.forecast.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.tcs.cba.weatherforecast.bean.WeatherDetailsBean;
import com.tcs.cba.weatherforecast.forecast.model.PredictWeather;
import com.tcs.cba.weatherforecast.utils.AlgorithmsUtil;
import static com.tcs.cba.weatherforecast.utils.WeatherForecastConstants.*;

/**
 * <p>
 * The core logic for this model lies in finding the range in which any weather
 * factors might vary and predicting an approximate value which satisfies the
 * mean-variance-standard deviation range and yet adheres to the weather
 * distribution pattern spread over historical data for any specific date.
 * </p>
 * 
 * @author Rajish R
 * 
 */
public class VarianceModelImpl implements PredictWeather {

	private Map<Integer, List<WeatherDetailsBean>> historyMap;
	
	public VarianceModelImpl() {
		
	}

	public VarianceModelImpl(Map<Integer, List<WeatherDetailsBean>> historyMap) {
		this.historyMap = historyMap;
	}

	/**
	 * <p>
	 * Method forecasts the weather factors for any specific day of the year.
	 * </p>
	 */
	public String findForecastForADate(int day, int year) {

		List<WeatherDetailsBean> historyDayList = historyMap.get(day);
		if (day == 366) {
			historyDayList.addAll(historyMap.get(day - 1));
		}
		List<WeatherDetailsBean> filteredList = pickHistoryDays(historyDayList, 3);
		return findWeatherInfo(filteredList, year);
	}

	/**
	 * <p>
	 * Method calculates the approximate weather factors forecasted for any
	 * specific date. Global warming factor is an optional factor which would
	 * affect the temperature on the long run.
	 * </p>
	 * 
	 * @param filteredList
	 * @param year
	 * @return
	 */
	public String findWeatherInfo(List<WeatherDetailsBean> filteredList, int year) {
		WeatherDetailsBean wdBean = null;
		int sizeOfArray = filteredList.size();
		double[] temperatureArray = new double[sizeOfArray];
		double[] pressureArray = new double[sizeOfArray];
		double[] humidityArray = new double[sizeOfArray];
		for (int i = 0; i < sizeOfArray; i++) {
			wdBean = filteredList.get(i);
			temperatureArray[i] = wdBean.getTemperature();
			pressureArray[i] = wdBean.getPressure();
			humidityArray[i] = wdBean.getHumidity();
		}

		double globalWarmingFactor = AlgorithmsUtil.findGlobalWarmingFactor(year - 2015);
		double forecastedTemperature = AlgorithmsUtil.findResultantValue(temperatureArray) + globalWarmingFactor;
		double forecastedPressure = AlgorithmsUtil.findResultantValue(pressureArray);

		int forecastedHumidity = (int) AlgorithmsUtil.findResultantValue(humidityArray);

		double roundedTemperature = roundedValue(forecastedTemperature);
		String tempRounded = roundedTemperature > 0 ? "+" + roundedTemperature : String.valueOf(roundedTemperature);
		String weatherType = getWeatherType(roundedTemperature, forecastedHumidity);
		return weatherType + PIPE_DELIMITER + tempRounded + PIPE_DELIMITER + roundedValue(forecastedPressure)
				+ PIPE_DELIMITER + forecastedHumidity;
	}

	/**
	 * Method helps predict the weather type based on the weather factors.
	 * 
	 * @param temperature
	 * @param humidity
	 * @return
	 */
	private String getWeatherType(double temperature, int humidity) {
		String weatherType = null;

		// conditions governing weatherType.
		if (temperature > 20 && humidity > 70 && humidity < 80) {
			weatherType = WEATHER_CLOUDY;
		} else if (temperature > 20 && humidity >= 80) {
			weatherType = WEATHER_RAINY;
		} else if (temperature >= 20 && temperature <= 24) {
			weatherType = WEATHER_WARM;
		} else if (temperature < 20 && temperature >= 14) {
			weatherType = WEATHER_COOL;
		} else if (temperature < 14) {
			weatherType = WEATHER_COLD;
		} else {
			weatherType = WEATHER_HOT;
		}

		return weatherType;

	}

	/**
	 * <p>
	 * Method returns the value rounded to 1 decimal place.
	 * </p>
	 * 
	 * @param forecastedValue
	 * @return
	 */
	private double roundedValue(double forecastedValue) {
		double roundValue = (double) Math.round(forecastedValue * 10) / 10;
		return roundValue;
	}

	/**
	 * <p>
	 * Method helps in filtering arbitrarily filtered list of history objects
	 * for a day and returns back the filtered list of objects.
	 * </p>
	 * 
	 * @param completeList
	 * @param n
	 * @return
	 */
	public List<WeatherDetailsBean> pickHistoryDays(List<WeatherDetailsBean> completeList, int n) {
		List<WeatherDetailsBean> copy = new LinkedList<WeatherDetailsBean>(completeList);
		Collections.shuffle(copy);
		return copy.subList(0, n);
	}

}
