/**
 * Copyright (c) 2016, Rajish R. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package com.tcs.cba.weatherforecast.forecast.impl;

import static com.tcs.cba.weatherforecast.utils.WeatherForecastConstants.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.tcs.cba.weatherforecast.bean.WeatherDetailsBean;
import com.tcs.cba.weatherforecast.exception.WeatherForecastException;
import com.tcs.cba.weatherforecast.forecast.model.PredictWeather;
import com.tcs.cba.weatherforecast.jaxb.Location;

/**
 * <p>
 * Executable thread class with the business logic to predict the weather for
 * one location per thread.
 * </p>
 * 
 * @author Rajish R
 * 
 */
public class ForecastThread implements Runnable {

	private String historyDir;
	private String locationCode;
	private String coordinates;
	private String elevation;
	private String mode;
	private String startDate;
	private String endDate;

	private Calendar cal;
	private SimpleDateFormat sdf;

	private Map<Integer, List<WeatherDetailsBean>> historyMap;

	public ForecastThread(Location location, String mode, String startDate, String endDate) {
		this.locationCode = location.getIatacode();
		this.coordinates = location.getCordinates();
		this.historyDir = location.getHistorydata().getDir();
		this.elevation = String.valueOf(location.getAltitude().intValue());
		this.mode = mode;
		this.startDate = startDate;
		this.endDate = endDate;

		cal = new GregorianCalendar();
		sdf = new SimpleDateFormat();
		sdf.setLenient(false);

	}

	public void run() {
		try {
			forcastWeatherReport();
		} catch (Exception exception) {
			exception.printStackTrace();
		}

	}

	/**
	 * <p>
	 * Method starts the processing of the thread.
	 * </p>
	 * 
	 * @throws IOException
	 * @throws ParseException
	 * @throws WeatherForecastException
	 * @throws InterruptedException
	 */
	private void forcastWeatherReport() throws IOException, ParseException, WeatherForecastException,
			InterruptedException {
		historyMap = new TreeMap<Integer, List<WeatherDetailsBean>>();
		loadHistoryDataFromDirectory();
		PredictWeather predictObj = new VarianceModelImpl(historyMap);
		if (FORECAST_ALL_FUTURE_DATES.equalsIgnoreCase(mode)) {
			findForecastForFutureDates(predictObj);
		} else if (FORECAST_DATE_RANGE.equalsIgnoreCase(mode)) {
			findForecastForADateRange(predictObj);
		} else if (FORECAST_DATE.equalsIgnoreCase(mode)) {
			int day = findDay(startDate);
			int year = getYear(getFormattedDate(startDate));
			printReport(predictObj.findForecastForADate(day, year), getFormattedDate(startDate));
		} else {
			throw new WeatherForecastException("use the right mode [-d,-r,-a]");
		}
	}

	/**
	 * <p>
	 * Method helps find the weather forecast between a given start and end
	 * date.
	 * </p>
	 * 
	 * @param predictObj
	 *            - object having the prediction process logic.
	 * @throws ParseException
	 * @throws InterruptedException
	 */
	private void findForecastForADateRange(PredictWeather predictObj) throws ParseException, InterruptedException {
		sdf.applyPattern(INPUT_DATE_PATTERN);
		Date beginDate = sdf.parse(startDate);
		Date stopDate = sdf.parse(endDate);
		Date tempDate = beginDate;
		Calendar c = Calendar.getInstance();
		c.setTime(beginDate);

		while (true) {
			int theDay = getDayOfYear(tempDate);
			int year = getYear(tempDate);
			printReport(predictObj.findForecastForADate(theDay, year), tempDate);
			c.add(Calendar.DATE, 1);
			tempDate = c.getTime();
			if (stopDate.before(tempDate)) {
				break;
			}
		}
	}

	/**
	 * <p>
	 * Method helps forecast weather for all future dates including the current
	 * date
	 * </p>
	 * 
	 * @param predictObj
	 * @throws ParseException
	 * @throws InterruptedException
	 */
	private void findForecastForFutureDates(PredictWeather predictObj) throws ParseException, InterruptedException {
		Date todaysDate = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(todaysDate);
		while (true) {
			int today = getDayOfYear(todaysDate);
			int year = getYear(todaysDate);
			printReport(predictObj.findForecastForADate(today, year), todaysDate);
			c.add(Calendar.DATE, 1);
			todaysDate = c.getTime();
		}
	}

	/**
	 * <p>
	 * Method used to print the report. It uses the standard output to print the
	 * result.
	 * </p>
	 * 
	 * @param predictedWeather
	 *            -weather prediction factors weather
	 *            type|temperature|pressure|relative humidity.
	 * @param date
	 * @throws InterruptedException
	 */
	private void printReport(String predictedWeather, Date date) throws InterruptedException {
		Thread.sleep(60);
		sdf.applyPattern(DATE_TIME_PATTERN);
		String reportStatement = locationCode + PIPE_DELIMITER + coordinates + COMMA + elevation + PIPE_DELIMITER
				+ sdf.format(date) + PIPE_DELIMITER + predictedWeather;
		
		System.out.println(reportStatement);

	}

	/**
	 * <p>
	 * Utility method finds the day of the year for a particular date
	 * </p>
	 * 
	 * @param dateVal
	 *            - the date in String format.
	 * @return day of the year in int format.
	 * @throws ParseException
	 */
	private int findDay(String dateVal) throws ParseException {
		Date dateObj = getFormattedDate(dateVal);
		int dayOfYear = getDayOfYear(dateObj);
		return dayOfYear;

	}

	/**
	 * <p>
	 * Method helps load History date from a directory specific to a location.
	 * </p>
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	private void loadHistoryDataFromDirectory() throws IOException, ParseException {
		File historyDirectory = new File(historyDir);
		File[] historyFiles = null;
		if (historyDirectory.exists() && historyDirectory.isDirectory()) {
			historyFiles = historyDirectory.listFiles();
			for (int i = 0; i < historyFiles.length; i++) {
				BufferedReader bufferedReader = new BufferedReader(new FileReader(historyFiles[i]));
				loadHistoryData(bufferedReader);
				if (bufferedReader != null) {
					bufferedReader.close();
				}
			}
		}
	}

	/**
	 * <p>
	 * Method helps load history data from each file read into the
	 * bufferedReader
	 * </p>
	 * 
	 * @param bufferedReader
	 *            - holds the data read for a file in a directory particular to
	 *            a location for a specific year.
	 * @throws IOException
	 * @throws ParseException
	 */
	private void loadHistoryData(final BufferedReader bufferedReader) throws IOException, ParseException {

		String weatherInfo = null;
		while (bufferedReader.ready()) {
			weatherInfo = bufferedReader.readLine();
			String[] lineSplit = weatherInfo.split(COMMA, -1);
			WeatherDetailsBean wdBean = setWeatherDetails(lineSplit);
			addWeatherDetails(wdBean);
		}
	}

	/**
	 * <p>
	 * Method adds the bean objects to a Map having a tuple each for a specific
	 * day of a year for eg. 'n'th day of the year would be present with a key
	 * 'n' where n is an Integer
	 * </p>
	 * 
	 * @param wdBean
	 */
	private void addWeatherDetails(WeatherDetailsBean wdBean) {
		List<WeatherDetailsBean> wdBeanList = null;
		int dayOfYear = wdBean.getDayOfYear();
		if (historyMap.containsKey(dayOfYear)) {
			wdBeanList = historyMap.get(dayOfYear);
		} else {
			wdBeanList = new ArrayList<WeatherDetailsBean>();
		}
		wdBeanList.add(wdBean);
		historyMap.put(dayOfYear, wdBeanList);
	}

	/**
	 * <p>
	 * Method helps to populate attributes of the bean object.
	 * </p>
	 * 
	 * @param lineSplit
	 *            - split data for each line split on delimiter comma(',').
	 * @return- bean to be added to the list.
	 * @throws ParseException
	 */
	private WeatherDetailsBean setWeatherDetails(String[] lineSplit) throws ParseException {
		WeatherDetailsBean wdBean = new WeatherDetailsBean();

		Date formattedDate = getFormattedDate(lineSplit[0]);

		wdBean.setDayOfYear(getDayOfYear(formattedDate));
		wdBean.setLocationCode(this.locationCode);
		wdBean.setCoordinates(this.coordinates);
		wdBean.setTemperature(Double.parseDouble(lineSplit[1]));
		wdBean.setHumidity(Double.parseDouble(lineSplit[2]));
		wdBean.setPressure(Double.parseDouble(lineSplit[3]));
		wdBean.setWeatherType(lineSplit[4].trim());
		wdBean.setYear(getYear(formattedDate));

		return wdBean;
	}

	/**
	 * <p>
	 * Returns a Date object which is the formatted date from the string input.
	 * </p>
	 * 
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public Date getFormattedDate(String date) throws ParseException {
		sdf.applyPattern(INPUT_DATE_PATTERN);
		Date dateInstance = sdf.parse(date);
		return dateInstance;

	}

	/**
	 * <p>
	 * Method returns the count of the day of the year for a particular date
	 * </p>
	 * 
	 * @param dateInstance
	 * @return
	 * @throws ParseException
	 */
	public int getDayOfYear(Date dateInstance) throws ParseException {
		cal.setTime(dateInstance);
		return cal.get(Calendar.DAY_OF_YEAR);
	}

	/**
	 * <p>
	 * Method formats the date to extract the year only.
	 * </p>
	 * 
	 * @param dateInstance
	 * @return
	 */
	public int getYear(Date dateInstance) {
		sdf.applyPattern(YEAR_PATTERN);
		return Integer.parseInt(sdf.format(dateInstance));
	}

}
