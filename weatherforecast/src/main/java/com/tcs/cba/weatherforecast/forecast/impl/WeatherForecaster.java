/**
 * Copyright (c) 2016, Rajish R. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package com.tcs.cba.weatherforecast.forecast.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.tcs.cba.weatherforecast.jaxb.Location;
import com.tcs.cba.weatherforecast.jaxb.Locations;
import com.tcs.cba.weatherforecast.utils.WeatherForecastConstants;

/**
 * <p>
 * This is the trigger/Main class. When called upon it accepts 2 to 4 parameters
 * based on the mode(parameter 1). args[0]- Mode parameter [-a => Forecasting
 * for all future dates including today. -d => Forecasting for a specific date
 * queried for. -r => Forecasting for a range of dates between queried start and
 * end dates] args[1]- absolute path of properties file
 * (weatherforecast/src/main/resources/config/weatherinfo.properties) args[2]-
 * for mode -d => specific date to be forecasted for mode -r => starting date of
 * forecast. args[3]- for mode -r => end date of forecast
 * </p>
 * 
 * @author Rajish R
 * 
 */
public class WeatherForecaster {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		WeatherForecaster forecastObj = new WeatherForecaster();
		ExecutorService executorService = null;
		String mode = args[0];
		String propertiesPath = args[1];
		String startDate = null;
		String endDate = null;

		WeatherForecastConstants.loadProperties(propertiesPath);

		if (args.length > 2) {
			startDate = args[2];
		}
		if (args.length > 3) {
			endDate = args[3];
		}
		try {
			List<Location> locationList = forecastObj.loadLocationSettings(WeatherForecastConstants.CONFIG_LOCATION);
			if (locationList != null && locationList.size() > 0) {
				executorService = Executors.newFixedThreadPool(locationList.size());
				for (Location location : locationList) {
					final ForecastThread forecastThreadObj = new ForecastThread(location, mode, startDate, endDate);
					final Thread locationThread = new Thread(forecastThreadObj, location.getCity());
					executorService.execute(locationThread);
				}
			}
		} finally {
			executorService.shutdown();
		}
	}

	/**
	 * <p>
	 * Method loads the configuration path, parsing the same using JAXB.
	 * </p>
	 * 
	 * @param configPath
	 * @return
	 */
	private List<Location> loadLocationSettings(String configPath) {
		File configFile = null;
		Locations locationList = null;
		try {

			configFile = new File(configPath);
			final JAXBContext jaxbContext = JAXBContext.newInstance(Locations.class);
			final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			locationList = (Locations) jaxbUnmarshaller.unmarshal(configFile);

		} catch (JAXBException exception) {
			exception.printStackTrace();
		}
		return (locationList == null) ? null : locationList.getLocation();
	}

}
