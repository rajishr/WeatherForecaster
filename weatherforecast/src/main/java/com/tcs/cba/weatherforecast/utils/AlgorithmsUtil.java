/**
 * Copyright (c) 2016, Rajish R. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package com.tcs.cba.weatherforecast.utils;

/**
 * <p>
 * The foundation of this model is to bank upon historical weather data to plot
 * the future weather factors based on the past trends.
 * 
 * This model is a hybrid of Deterministic and Probabilistic approach. This
 * method is chosen since the change in weather data must be evident and
 * logical.
 * 
 * This is a mathematical model. The model tries to figure out the arbitrarily
 * selected historical values of temperature in C, pressure in hPa and relative
 * humidity. The mean for the filtered data is the optimal level Mean(μ) =
 * ∑ij(X(i)+X(i+1)....X(j))/j ; where j is the total number of samples
 * 
 * Assuming the mean as the optimal level the variance is calculated:
 * Variance(σ^2) = ∑ij[f(X(ij),μ)]/j ; where f(X(ij),μ) = (Xi-μ)^2
 * 
 * The variance would help knowing the data distribution. The exact range of
 * optimal data variance is known by finding the standard deviation.
 * StandardDeviation(σ)=SQRT(Variance) ; where SQRT is the square root function.
 * 
 * The net polarity of the variance is assumed to be the summation of the
 * individual data variance to the mean value. The product of net polarity(+1 or
 * -1) and standard deviation is added to the mean value to obtain the weather
 * factors for any specific date.
 * 
 * The global warming coefficient and its effect on the model is added to bring
 * in the realistic view of the model on the long run.
 * </p>
 * 
 * @author Rajish R
 * 
 */
public class AlgorithmsUtil {

	/**
	 * <p>
	 * Method provides the resultant value after the model is processed.
	 * </p>
	 * 
	 * @param values
	 * @return
	 */
	public static double findResultantValue(double[] values) {
		
		double mean = findMean(values);
		double variancePolarity = findVariancePolarity(values, mean);
		double variance = findVariance(values, mean);
		double standardDeviation = findStandardDeviation(variance);
		if (variancePolarity >= 0) {
			variancePolarity = 1;
		} else {
			variancePolarity = -1;
		}
		return (mean + (variancePolarity * standardDeviation));

	}

	/**
	 * <p>
	 * Method helps in finding the standard deviation.
	 * </p>
	 * 
	 * @param variance
	 * @return
	 */
	public static double findStandardDeviation(double variance) {
		
		return Math.sqrt(variance);
	}

	/**
	 * <p>
	 * Methods helps derive the overall polarity of variance (positive or
	 * negative)
	 * </p>
	 * 
	 * @param values
	 * @param mean
	 * @return
	 */
	public static double findVariancePolarity(double[] values, double mean) {
		
		double polarity = 0;
		for (int i = 0; i < values.length; i++) {
			polarity = polarity + (values[i] - mean);
		}
		return polarity;
	}

	/**
	 * <p>
	 * Method helps derive the variance for a set of values.
	 * </p>
	 * 
	 * @param values
	 * @param mean
	 * @return
	 */
	public static double findVariance(double[] values, double mean) {
		
		double diff = 0;
		for (int i = 0; i < values.length; i++) {
			diff = diff + Math.pow((values[i] - mean), 2);
		}
		return diff / values.length;
	}

	/**
	 * <p>
	 * Method helps derive the mean for a set of values.
	 * </p>
	 * 
	 * @param values
	 * @return
	 */
	public static double findMean(double[] values) {
		double sum = 0;
		for (int i = 0; i < values.length; i++) {
			sum = sum + values[i];
		}
		return sum / values.length;
	}

	/**
	 * Optimal factor rise in temperature due to Global warming year after year.
	 * Note: The formula for deriving this factor is derived based on
	 * self-research and valid test cases from Internet which predicts 0.4-2.0
	 * degree rise in temperature in the upcoming 40-50 years.
	 * 
	 * If this coefficient GLOBAL_WARMING_COEFF is set to 0 the effect of Global
	 * warming factor on the model would be ignored.
	 * 
	 * @param yearDifference
	 * @return
	 */
	public static double findGlobalWarmingFactor(int yearDifference) {
		
		if (WeatherForecastConstants.GLOBAL_WARMING_COEFF != 0d) {
			return ((yearDifference / WeatherForecastConstants.GLOBAL_WARMING_COEFF) * Math.log10(2));
		}
		return 0;
	}
}
