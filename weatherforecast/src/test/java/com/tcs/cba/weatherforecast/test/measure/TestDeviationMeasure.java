/**
 * Copyright (c) 2016, Rajish R. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.tcs.cba.weatherforecast.test.measure;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.tcs.cba.weatherforecast.utils.AlgorithmsUtil;

public class TestDeviationMeasure {

	private double result_mean = 0d;
	private double result_variance = 0d;
	private double result_deviation = 0d;

	private double decimal_round_off = 0.001d;
	double[] sample_temperature = new double[4];

	

	@Before
	public void intializeValues() throws Exception {

		double[] testData = { 22, 39, 27, 22 };
		
		result_mean = 27.5;
		result_variance = 48.25;
		result_deviation = 6.946;
		sample_temperature = testData;

	}

	@Test
	public void calculateMeanSuccess() {

		double mean_value = AlgorithmsUtil.findMean(sample_temperature);
		assertEquals(this.result_mean, mean_value, decimal_round_off);
	}

	@Test
	public void calculateVarianceSuccess() {

		double variance_value = AlgorithmsUtil.findVariance(sample_temperature, result_mean);
		assertEquals(this.result_variance, variance_value, decimal_round_off);
	}

	@Test
	public void calculateDeviationSuccess() {

		double deviation_value = AlgorithmsUtil.findStandardDeviation(result_variance);
		assertEquals(this.result_deviation, deviation_value, decimal_round_off);
	}
	
}
