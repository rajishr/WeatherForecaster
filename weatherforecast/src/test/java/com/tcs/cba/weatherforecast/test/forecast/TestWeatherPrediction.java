/**
 * Copyright (c) 2016, Rajish R. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package com.tcs.cba.weatherforecast.test.forecast;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import com.tcs.cba.weatherforecast.bean.WeatherDetailsBean;
import com.tcs.cba.weatherforecast.forecast.impl.VarianceModelImpl;

public class TestWeatherPrediction {

	
	public List<WeatherDetailsBean> historyList = null;
	public String result_Weather_Info = null;
	

	@Before
	public void intializeValues() throws Exception {
		historyList = new ArrayList<WeatherDetailsBean>();
		result_Weather_Info = "RAINY|+22.0|1096.3|82";
		initializeListOfHistory();

	}

	/**
	 * Intializes dummy value to be placed as history list.
	 */
	private void initializeListOfHistory() {
		
		WeatherDetailsBean wdBean1 = new WeatherDetailsBean();
		
		wdBean1.setCoordinates("-33.86,151.21");
		wdBean1.setDayOfYear(12);
		wdBean1.setLocationCode("SYD");
		wdBean1.setHumidity(72d);
		wdBean1.setPressure(1100d);
		wdBean1.setTemperature(26d);
		wdBean1.setWeatherType("CLOUDY");
		wdBean1.setYear(2010);
		
		WeatherDetailsBean wdBean2 = new WeatherDetailsBean();
		
		wdBean2.setCoordinates("-33.86,151.21");
		wdBean2.setDayOfYear(12);
		wdBean2.setLocationCode("SYD");
		wdBean2.setHumidity(65d);
		wdBean2.setPressure(1070d);
		wdBean2.setTemperature(22d);
		wdBean2.setWeatherType("WARM");
		wdBean2.setYear(2013);
		
		WeatherDetailsBean wdBean3 = new WeatherDetailsBean();
		
		wdBean3.setCoordinates("-33.86,151.21");
		wdBean3.setDayOfYear(12);
		wdBean3.setLocationCode("SYD");
		wdBean3.setHumidity(85d);
		wdBean3.setPressure(1082d);
		wdBean3.setTemperature(23d);
		wdBean3.setWeatherType("RAINY");
		wdBean3.setYear(2015);
		
		historyList.add(wdBean1);
		historyList.add(wdBean2);
		historyList.add(wdBean3);
		
	}

	@Test
	public void foreCastWeatherSuccess() {

		VarianceModelImpl vImpl = new VarianceModelImpl();
		String weatherInfo = vImpl.findWeatherInfo(historyList, 2015);
		assertEquals(result_Weather_Info, weatherInfo);
	}

}
